from setuptools import setup, find_packages

setup(
    name='orgtoanki',
    packages=find_packages(),
    scripts=['bin/orgtoanki']
)
