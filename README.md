# org-to-anki

A Python tool for generating Anki flashcards from .org files

## Requirements

You'll need the following:

- Python3
- [genanki](https://github.com/kerrickstaley/genanki)

## Installation

``` bash

git clone https://gitlab.com/qualisign/org-to-anki.git  

pip3 install ./org-to-anki

```

## Commands

#### basic syntax:

``` bash

orgtoanki [NAME OF NEW DECK] [PATH TO .ORG FILE] [--OPTIONS]
	
```

optional flags:

``` bash

-d # delimiter (defaults to *)
-f # fields (defaults to 'front,back')

```

## Example

#### **`delay-loss-throughput.org`**

``` org
* FRONT what is throughput?
* BACK the amount of data per second that can be transmitted
* FRONT what are the factors of total nodal delay?
* BACK nodal processing, queueing, transmission, and propagation delay
* FRONT what type of delay includes the time required to examine the packet’s header and determine where to direct thepacket 
* BACK processing delay
* FRONT what does processing delay include besides examining header/determining where to direct the packet?
* BACK checking for bit-level errors that may have occurred in transmission of the packet
...
```


``` bash

orgtoanki "computer networking::internet basics::delay, loss, and throughput" "delay-loss-throughput.org"

```

![delay-loss-throughput](anki-example.png "Result")
