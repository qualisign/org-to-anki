# POSITIVE TEST CASES

# user writes an .org file with a set of semantically marked bullets that demarcate card questions (fields being marked 'front' or 'back'

# user enters into the shell `orgtoanki <path_of_source> --fields="front,back"` and sees that an anki deck has been generated in their current directory (with `front` and `back` being the default fields)

# user enters into the shell `orgtoanki <path_of_source> <path_to_target>` and sees that an anki deck has been generated where they specified the target

# user enters into the shell `orgtoanki <path_of_source> <path_to_target>` and sees that an anki deck has been generated where they specified the target

# user enters into the shell `orgtoanki <path_of_source> <path_to_target>` and sees that an anki deck has been generated where they specified the target

# user writes an .org file with a set of semantically marked bullets that demarcate card questions (fields being marked `field1, field2, field3`

# user enters into the shell `orgtoanki <path_of_source> --fields="field1, field2, field3"` and sees that an anki deck has been generated in their current directory (with `front` and `back` being the default fields)

# NEGATIVE TEST CASES

# user runs command with a non-org file.  receives error message.

# user runs command without specifying fields.  receives error message.

# user writes an .org file with an unbalanced set of demarcated bullets

# user enters into the shell `orgtoanki <path_of_source>` and sees an error message saying that the bullets are unbalanced
