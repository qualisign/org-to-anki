from orgtoanki.api import create_model, create_note, create_deck, create_package
import genanki

model=create_model(['front', 'back'])
def test_create_model():  # string => Model    
    assert type(model) is genanki.Model

def test_create_note(): # 
#    assert type(create_note)
    example=['which layers does a router implement', '1,2,3']
    assert type(create_note(example, model)) is genanki.Note

def test_create_notes(): # returns a list of notes
    pass

def test_create_deck():  # list of Notes => Deck
    pass

def test_create_package():  # Deck => Package
    pass
    
    
