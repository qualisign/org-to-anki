from orgtoanki.api import org_to_list

example1_text = """
~ FRONT here is the front of a card
~ TODO this is some other tag
~ BACK here is the back of a card
~ FRONT here is the front of a card
~ BACK here is the back of a card
~ FRONT here is the front of a card
~ BACK here is the back of a card
"""

example1 = {
    "text":  example1_text,
    "fields": ["front", "back"],
    "delimiter": "~"
}

def test_org_to_list():  # string => list

#    assert type(org_to_list(example1["text"], example1["fields"]), example1["delimiter"]) is list
    pass
    
def test_list_of_lists():
    # each list should be as long as the fields length
    list_list=org_to_list(example1["text"], example1["fields"], example1["delimiter"])    
    for t in list_list:
        assert len(t) == len(example1["fields"])
        
def test_raises_order_exception():
    pass

def test_raises_field_count_exception():
    pass
    
            
