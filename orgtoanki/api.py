import re
import genanki

def org_to_list(src, fields, delimiter):
    # read contents of org file
    # chunk text by fields
    # split chunks
    # return a list with size multiple of fields length
    rx=""
    result=[]
    field_counts={}    
    for field in fields:
        s = delimiter+' '+field.upper()
        field_counts[field] = src.count(s)
    for field in fields:
        rx+='\\'+delimiter+' '+field.upper()+'(.*?)'        
    rx*=field_counts[fields[0]]
    rx=rx[0:-2]+rx[-1:]  # make last group greedy (remove ?)
    field_count=field_counts[fields[0]]    
    group_count = len(fields)*field_count
    field_matches = re.search(rx, src, flags=re.DOTALL)    
    # make sure fields are balanced
    for field in fields:
        assert field_count == field_counts[field]        
    position=1
    for i in range(field_count):
        tup = [None]*(len(fields))
        for j in range(len(fields)):
            tup[j] = field_matches.group(position)
            position+=1
        result.append(tup)
    return result

def create_model(fields):
    model=genanki.Model(
        1607392319,
        'front_back_model',
        fields=[{'name': x} for x in fields],        
        templates=[
            {
                'name': 'Card 1',
                'qfmt': '{{'+str(fields[0])+'}}',
                'afmt': '{{FrontSide}}<hr id="'+str(fields[1])+'">{{'+str(fields[1])+'}}',
            },
        ])
    return model

def create_notes(card_list, fields):
    model=create_model(fields)
    result=[]

    def create_note(contents, model):
        note=genanki.Note(
            model=model,
            fields=contents
        )
        return note
    
    for card in card_list:
        note=create_note(card, model)
        result.append(note)
    return result


def create_deck(note_list, name):
    deck = genanki.Deck(
        2059400110,
        name)
    for note in note_list:
        deck.add_note(note)
    return deck

def create_package(name, org_src, fields_string, delimiter):
    fields=fields_string.split(',')
    org_file=open(org_src, 'r')
    text=org_file.read()
    print(text)
    card_list=org_to_list(text, fields, delimiter)
    note_list=create_notes(card_list, fields)
    deck=create_deck(note_list, name)
    genanki.Package(deck).write_to_file(name+'.apkg')

if __name__=="__main__":
    example1_text = """
~ FRONT here is the front of a card
~ TODO this is some other tag
~ BACK here is the back of a card
~ TODO j
~ FRONT here is the front of a card
~ BACK here is the back of a card
~ FRONT here is the front of a card
~ BACK here is the back of a card
"""
    org_to_list(example1_text, ['front', 'back'], '~')
